package com.bajuglair.caillou.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MyProduct {
    @Id
    private String id;
    private String barCode;
    private String nutritionClass;
    private String color;
    private String name;
    private int nutrition_score;
}

//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


//@JsonDeserialize

