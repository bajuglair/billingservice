package com.bajuglair.caillou.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "nutrition_score")
public class NutritionScore {
    @Id
    int id;
    @Column(name="class")
    String nutritionClass;
    int lowerBound;
    int upper_bound;
    String color;

}
