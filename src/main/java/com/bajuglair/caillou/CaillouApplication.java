package com.bajuglair.caillou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaillouApplication {
	public static void main(String[] args){
		System.out.println("launching");
		SpringApplication.run(CaillouApplication.class, args);
	}
}
