package com.bajuglair.caillou.Repository;

import com.bajuglair.caillou.Entities.MyProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BasketRepository  extends JpaRepository<MyProduct, Long> {

}
