package com.bajuglair.caillou.Repository;

import com.bajuglair.caillou.Entities.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends JpaRepository<Rule, Long> {
    /**
     * Get rule corresponding to name with min_bound
     * @param name
     * @param min_bound
     * @return Rule object
     */
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM RULE WHERE name=:name AND min_bound<=:min_bound ORDER BY min_bound DESC")
    Rule getRuleByBound(String name, float min_bound);
}
