package com.bajuglair.caillou.Repository;

import com.bajuglair.caillou.Entities.NutritionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionScoreRepository extends JpaRepository<NutritionScore, Long> {
    /**
     * Get NutritionScore of points
     * @param points
     * @return NutritionScore object
     */
    @Query("SELECT n FROM NutritionScore n WHERE lower_bound<=?1 AND upper_bound>=?1")
    NutritionScore getNutritionScoreByPoints(int points);
}
