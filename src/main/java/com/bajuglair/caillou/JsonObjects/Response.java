package com.bajuglair.caillou.JsonObjects;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private String code;
    private Product product;
    @JsonProperty("status_verbose")
    private String statusVerbose;
}
