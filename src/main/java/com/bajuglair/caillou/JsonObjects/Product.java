package com.bajuglair.caillou.JsonObjects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private String _id;
    private String generic_name;
    private float energy_100g;
    @JsonProperty("saturated-fat_100g")
    private float saturated_fat_100g ;
    private float sugars_100g;
    private float salt_100g;
    private float fiber_100g;
    private float proteins_100g;


    @Override
    public String toString() {
        return "Customer{" +
            "_id=" + _id +
            ",generic_name=" + generic_name +
            ",energy=" + energy_100g +
            ", saturated_fat=" + saturated_fat_100g +
            ", sugars=" + sugars_100g +
            ", salt=" + salt_100g +
            ", fiber=" + fiber_100g +
            ", proteins=" + proteins_100g +
            '}';
    }
}
