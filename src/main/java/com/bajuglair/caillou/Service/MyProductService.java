package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Entities.NutritionScore;
import com.bajuglair.caillou.Entities.MyProduct;
import com.bajuglair.caillou.JsonObjects.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class MyProductService {
    @Autowired
    private RuleService ruleService;

    @Autowired
    private NutritionScoreService nutritionScoreService;

    /**
     * Create MyProduct from response
     * @param response
     * @return MyProduct
     */
    @Transactional
    public MyProduct getMyProductFromResponse(Response response){
        // Get nutrition score of myProduct
        int points = ruleService.computeScore(response.getProduct());

        // Get its color and class
        NutritionScore nutritionScore = nutritionScoreService.getNutritionScoreByPoints(points);

        // Create myProduct to be returned
        MyProduct myProduct = new MyProduct();
        myProduct.setNutrition_score(points);
        myProduct.setColor(nutritionScore.getColor());
        myProduct.setNutritionClass(nutritionScore.getNutritionClass());
        myProduct.setName(response.getProduct().getGeneric_name());
        myProduct.setBarCode(response.getCode());
        myProduct.setId(response.getProduct().get_id());

        return myProduct;
    }

}
