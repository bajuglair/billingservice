package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Repository.NutritionScoreRepository;
import com.bajuglair.caillou.Entities.NutritionScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NutritionScoreService {

    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    /**
     * Return nutrition score based on points
     * @param points
     * @return NutritionScore
     */
    public NutritionScore getNutritionScoreByPoints(int points){
        return nutritionScoreRepository.getNutritionScoreByPoints(points);
    }
}


