package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Entities.MyProduct;
import com.bajuglair.caillou.Repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketService {

    @Autowired
    BasketRepository basketRepository;

    /**
     * return all MyProduct from basket
     * @return List<MyProduct>
     */
    public List<MyProduct> findAll(){
        return basketRepository.findAll();
    }

    /**
     * Save a MyProduct in the basket
     * @param myProduct
     */
    public void save(MyProduct myProduct){
        basketRepository.save(myProduct);
    }
}
