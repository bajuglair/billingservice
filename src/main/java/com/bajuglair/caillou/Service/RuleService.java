package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Entities.Rule;
import com.bajuglair.caillou.Repository.RuleRepository;
import com.bajuglair.caillou.JsonObjects.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class RuleService {

    @Autowired
    private RuleRepository ruleRepository;

    /**
     * Compute score of given product
     * @param product
     * @return score
     */
    @Transactional
    public int computeScore(Product product){
        int sum = 0;
        System.out.println(product.getSalt_100g());
        List<Rule> rules = new ArrayList<Rule>();
        rules.add(ruleRepository.getRuleByBound("salt_100g", product.getSalt_100g()));
        rules.add(ruleRepository.getRuleByBound("fiber_100g", product.getFiber_100g()));
        rules.add(ruleRepository.getRuleByBound("energy_100g", product.getEnergy_100g()));
        rules.add(ruleRepository.getRuleByBound("proteins_100g", product.getProteins_100g()));
        rules.add(ruleRepository.getRuleByBound("saturated-fat_100g", product.getSaturated_fat_100g()));
        rules.add(ruleRepository.getRuleByBound("sugars_100g", product.getSugars_100g()));

        // Compute score
        for(Rule r: rules){
            if(r.getComponent().equals("N")){
                sum += r.getPoints();
            }
            else if(r.getComponent().equals("P")){
                sum -= r.getPoints();
            }
        }
        return sum;
    }
}
