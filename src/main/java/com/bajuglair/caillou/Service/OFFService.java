package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Exceptions.OFFProductNotFoundException;
import com.bajuglair.caillou.JsonObjects.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OFFService {

    /**
     * Call Open Food Facts API and get Response
     * @param id
     * @return Response
     * @throws JsonProcessingException
     * @throws OFFProductNotFoundException
     */
    public Response getResponseFromOFFProduct(String id) throws JsonProcessingException, OFFProductNotFoundException {
        // Consume OpenFoodFacts
        RestTemplate restTemplate = new RestTemplate();
        String openFoodFactURL = "https://fr.openfoodfacts.org/api/v0/produit/" + id + ".json?fields=_id,generic_name,energy_100g,saturated-fat_100g,sugars_100g,salt_100g,fiber_100g,proteins_100g";
        ResponseEntity<String> offResponse = restTemplate.getForEntity(openFoodFactURL, String.class);

        // Create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // Read response json and convert to customer object
        Response response = objectMapper.readValue(offResponse.getBody(), Response.class);
        if(response.getStatusVerbose().equals("product not found")){
            throw new OFFProductNotFoundException(response.getStatusVerbose());
        }
        System.out.println(response.getProduct());
        return response;
    }

}
