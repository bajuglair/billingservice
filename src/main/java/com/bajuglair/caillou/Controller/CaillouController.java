package com.bajuglair.caillou.Controller;

import com.bajuglair.caillou.Exceptions.OFFProductNotFoundException;
import com.bajuglair.caillou.Service.BasketService;
import com.bajuglair.caillou.Service.MyProductService;
import com.bajuglair.caillou.Service.OFFService;
import com.bajuglair.caillou.JsonObjects.Response;
import com.bajuglair.caillou.Entities.MyProduct;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CaillouController {
    @Autowired
    private MyProductService myProductService;

    @Autowired
    private BasketService basketService;

    @Autowired
    private OFFService offService;

    @GetMapping(value = "/product/{id}")
    public ResponseEntity<MyProduct> getResourceGroupCost(@PathVariable String id) {

        // Call OpenFoodFacts
        Response response = null;
        try {
            response = offService.getResponseFromOFFProduct(id);
        } catch (JsonProcessingException e) {
            return ResponseEntity.internalServerError().build();
        }
        catch(OFFProductNotFoundException e){
            return ResponseEntity.notFound().build();
        }

        // Get MyProduct from Response
        MyProduct myProduct = myProductService.getMyProductFromResponse(response);

        // Add myProduct to basket
        basketService.save(myProduct);
        return ResponseEntity.ok(myProduct);
    }

    @GetMapping(value = "/basket")
    public ResponseEntity<List<MyProduct>> getBasket(){
        // Get all basket
        List<MyProduct> myProducts = basketService.findAll();
        return ResponseEntity.ok(myProducts);
    }
}
