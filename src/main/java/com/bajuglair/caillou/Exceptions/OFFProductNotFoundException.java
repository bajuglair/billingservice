package com.bajuglair.caillou.Exceptions;

public class OFFProductNotFoundException extends Exception{
    public OFFProductNotFoundException(String errorMessage){
        super(errorMessage);
    }
}
