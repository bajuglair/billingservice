package com.bajuglair.caillou.Service;


import com.bajuglair.caillou.Entities.NutritionScore;
import com.bajuglair.caillou.Entities.MyProduct;
import com.bajuglair.caillou.JsonObjects.Product;
import com.bajuglair.caillou.JsonObjects.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
public class MyProductServiceTest {
    @Mock
    private RuleService ruleService;

    @Mock
    private NutritionScoreService nutritionScoreService;

    @InjectMocks
    private MyProductService myProductService;

    @Test
    public void getMyProductFromResponseTest(){
        String code = "7622210449283";
        String name = "Cookies";
        Product product = new Product(code, name, 1, 2, 3, 4, 5, 6);
        String nutritionClass = "Bon";
        String color = "green";



        Response response = new Response(code, product, "product found");
        when(ruleService.computeScore(product))
            .thenReturn(10);
        when(nutritionScoreService.getNutritionScoreByPoints(10))
            .thenReturn(new NutritionScore(0, "Bon", 0, 10, "green"));

        MyProduct myProduct = myProductService.getMyProductFromResponse(response);
        assertEquals(code, myProduct.getBarCode());
        assertEquals(color, myProduct.getColor());
        assertEquals(code, myProduct.getId());
        assertEquals(name, myProduct.getName());
        assertEquals(10, myProduct.getNutrition_score());
        assertEquals(nutritionClass, myProduct.getNutritionClass());
    }

}
