package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Repository.NutritionScoreRepository;
import com.bajuglair.caillou.Entities.NutritionScore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
public class NutritionScoreServiceTest {
    @Mock
    private NutritionScoreRepository nutritionScoreRepository;

    @InjectMocks
    private NutritionScoreService nutritionScoreService;

    /**
     * Test getNutritionScoreByPoints with correct param
     */
    @Test
    void getNutritionScoreByPointsTest(){
        Mockito
                .when(nutritionScoreRepository.getNutritionScoreByPoints(10))
                .thenReturn(new NutritionScore(0, "Bon", 0, 10, "green"));
        int points = 10;
        NutritionScore nutritionScore = nutritionScoreService.getNutritionScoreByPoints(points);
        assertTrue(nutritionScore.getLowerBound() <= points && nutritionScore.getUpper_bound() >= points);
    }

    /**
     * Test getNutritionScoreByPoints with bad points
     */
    @Test
    void getNutritionScoreByPointsBadTest(){
        Mockito
                .when(nutritionScoreRepository.getNutritionScoreByPoints(-10))
                .thenReturn(null);
        int points = -10;
        NutritionScore nutritionScore = nutritionScoreService.getNutritionScoreByPoints(points);
        assertNull(nutritionScore);
    }
}
