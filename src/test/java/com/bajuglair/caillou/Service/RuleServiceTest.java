package com.bajuglair.caillou.Service;

import com.bajuglair.caillou.Repository.RuleRepository;
import com.bajuglair.caillou.Entities.Rule;
import com.bajuglair.caillou.JsonObjects.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
public class RuleServiceTest {
    @Mock
    private RuleRepository ruleRepository;

    @InjectMocks
    private RuleService ruleService;

    /**
     * Test computeScore with correct params
     */
    @Test
    void computeScoreTest(){
        when(ruleRepository.getRuleByBound("energy_100g", 1350))
            .thenReturn(new Rule(0, "energy_100g", 2, 1340, "N"));
        when(ruleRepository.getRuleByBound("saturated-fat_100g", 5))
            .thenReturn(new Rule(1, "saturated-fat_100g", 5, 5, "N"));
        when(ruleRepository.getRuleByBound("sugars_100g", 19))
            .thenReturn(new Rule(2, "sugars_100g", 2, 18, "N"));
        when(ruleRepository.getRuleByBound("salt_100g", 10))
            .thenReturn(new Rule(3, "salt_100g", 4, 7, "N"));
        when(ruleRepository.getRuleByBound("fiber_100g", 40))
            .thenReturn(new Rule(4, "fiber_100g", 2, 39, "P"));
        when(ruleRepository.getRuleByBound("proteins_100g", 12))
            .thenReturn(new Rule(5, "", 1, 11, "P"));

        Product product = new Product("0", "Cookies", 1350, 5, 19, 10, 40, 12);
        assertEquals(10, ruleService.computeScore(product));
    }

}
