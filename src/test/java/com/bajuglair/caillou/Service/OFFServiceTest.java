package com.bajuglair.caillou.Service;


import com.bajuglair.caillou.Exceptions.OFFProductNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class OFFServiceTest {
    @Mock
    RestTemplate restTemplate;
    @InjectMocks
    OFFService offService;

    /**
     * Test offService throws OFFProductNotFoundException when OFF product not found
     */
    @Test
    public void getResponseFromOFFProductNotFoundTest(){
        String apiURL = "https://fr.openfoodfacts.org/api/v0/produit/111111111111111111.json?fields=_id,generic_name,energy_100g,saturated-fat_100g,sugars_100g,salt_100g,fiber_100g,proteins_100g";
        ResponseEntity<String> expected = new ResponseEntity<String>("\"{\"code\":\"111111111111111111\",\"status\":0,\"status_verbose\":\"product not found\"}\"", HttpStatus.OK);

        when(restTemplate.getForEntity(apiURL, String.class)).thenReturn(expected);

        assertThrows(OFFProductNotFoundException.class, () -> {
            offService.getResponseFromOFFProduct("111111111111111111");
        });

    }

}
