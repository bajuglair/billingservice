package com.bajuglair.caillou.Repository;

import com.bajuglair.caillou.Entities.NutritionScore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class NutritionScoreRepositoryTest {
    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    /**
     * Test result of getNutritionScoreByPointsTest with correct params
     */
    @Test
    public void getNutritionScoreByPointsTest(){
        int points = 10;
        NutritionScore nutritionScore = nutritionScoreRepository.getNutritionScoreByPoints(points);
        assertTrue(nutritionScore.getLowerBound() <= points && nutritionScore.getUpper_bound() >= points);
    }

    /**
     * Test result of getNutritionScoreByPointsTest with bad points
     */
    @Test
    public void getNutritionScoreByPointsBadPointTest(){
        int points = -10000;
        NutritionScore nutritionScore = nutritionScoreRepository.getNutritionScoreByPoints(points);
        assertNull(nutritionScore);
    }


}
