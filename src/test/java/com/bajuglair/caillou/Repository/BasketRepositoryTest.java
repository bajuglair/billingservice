package com.bajuglair.caillou.Repository;


import com.bajuglair.caillou.Entities.MyProduct;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class BasketRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BasketRepository basketRepository;

    @Test
    public void testFindAllTest(){
        entityManager.persist(new MyProduct("0", "0", "Bon", "green", "Cookies", 2));
        entityManager.persist(new MyProduct("1", "1", "Mauvais", "orange", "Chips", 20));
        var myProducts = basketRepository.findAll();
        assertEquals(2, myProducts.size());
    }

}
