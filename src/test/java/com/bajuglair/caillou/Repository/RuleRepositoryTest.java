package com.bajuglair.caillou.Repository;

import com.bajuglair.caillou.Entities.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RuleRepositoryTest {

    @Autowired
    private RuleRepository ruleRepository;

    /**
     * Test result of getRuleByBounds with correct params
     */
    @Test
    public void getRuleByBoundsTest(){
        String name = "salt_100g";
        int min_bound = 10;

        Rule rule = ruleRepository.getRuleByBound(name, min_bound);
        assertEquals(name, rule.getName());
        assertTrue(rule.getMin_bound() <= min_bound);
    }

    /**
     * Test result of getRuleByBounds when name is bad
     */
    @Test
    public void getRuleByBoundsBadNameTest(){
        String name = "none";
        int min_bound = 10;

        Rule rule = ruleRepository.getRuleByBound(name, min_bound);
        assertNull(rule);
    }

    /**
     * Test result of getRuleByBounds when min_bound is bad
     */
    @Test
    public void getRuleByBoundsBadBoundTest(){
        String name = "salt_100g";
        int min_bound = -10;

        Rule rule = ruleRepository.getRuleByBound(name, min_bound);
        assertNull(rule);

    }

}
