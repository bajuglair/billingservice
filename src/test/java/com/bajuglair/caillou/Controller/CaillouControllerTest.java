package com.bajuglair.caillou.Controller;

import com.bajuglair.caillou.Entities.MyProduct;
import com.bajuglair.caillou.Exceptions.OFFProductNotFoundException;
import com.bajuglair.caillou.Service.BasketService;
import com.bajuglair.caillou.Service.MyProductService;
import com.bajuglair.caillou.Service.OFFService;
import com.bajuglair.caillou.JsonObjects.Product;
import com.bajuglair.caillou.JsonObjects.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CaillouController.class)
public class CaillouControllerTest {
    @MockBean
    private MyProductService myProductService;

    @MockBean
    private BasketService basketService;

    @MockBean
    private OFFService offService;

    @Autowired
    private MockMvc mvc;


    /**
     * Test product route with existing id
     * @throws Exception
     */
    @Test
    public void getResponseFromOFFProductTest() throws Exception {
        when(offService.getResponseFromOFFProduct("1"))
            .thenReturn(new Response("1", new Product(), "product found"));

        mvc.perform((get("/api/product/1")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Test product route with not found id
     * @throws Exception
     */
    @Test
    public void getResponseFromOFFProductNotFoundTest() throws Exception {
        when(offService.getResponseFromOFFProduct("0"))
            .thenThrow(OFFProductNotFoundException.class);
        when(myProductService.getMyProductFromResponse(null))
            .thenReturn(null);

        mvc.perform(get("/api/product/0").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    /**
     * Test basket route
     */
    @Test
    public void getBasketTest() throws Exception {
        when(basketService.findAll())
            .thenReturn(new ArrayList<MyProduct>(Arrays.asList(new MyProduct("0", "0", "bon", "green", "Cookies", 4))));

        mvc.perform(get("/api/basket").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
