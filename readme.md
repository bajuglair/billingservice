# Caillou Java TP

## API endpoints
**/api/product/{id}**  
return product by id

**/api/basket**  
return all products consulted since start of API


## Swagger
Swagger route is reachable at  
/swagger-ui/index.html